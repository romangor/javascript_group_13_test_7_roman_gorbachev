export class Product {
  constructor(
    public name: string,
    public price: number,
    public quantity: number,
    public url: string,
  ) {}

  getPriceProduct() {
    return this.quantity * this.price;
  }
}




