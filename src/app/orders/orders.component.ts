import {Component, Input} from '@angular/core';
import { Product } from '../shared/product.model';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent  {
  @Input()  products: Product[] = [];

  deleteItem(index:number){
   this.products[index].quantity = 0;
 }

  getTotalPrice() {
   let totalPrice = 0;
   this.products.forEach(product => {
     totalPrice += product.getPriceProduct();
   });
   return totalPrice;
 }
}
