import {Component, EventEmitter, Input, Output} from '@angular/core';
import { Product } from '../shared/product.model';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent {
  @Input() products: Product[] = [];
  @Output() addQuantity = new EventEmitter();

  addQuantityProduct(index:number){
    this.products[index].quantity++;
    this.addQuantity.emit(this.products[index]);
  }
}
